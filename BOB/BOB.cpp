#include "BOB.h"
#include <Arduino.h>

BOB::BOB(int pinG, int pinRCK, int pinSCK, int pinSCLR, int pinD):BOB(pinG, pinRCK,pinSCK,pinSCLR,pinD, 1)
{

}


BOB::BOB(int pinG, int pinRCK, int pinSCK, int pinSCLR, int pinD, unsigned int bobNumber){
    _pinG=pinG;
    _pinSCLR=pinSCLR;
    _pinRCK=pinRCK;
    _pinSCK=pinSCK;
    _pinD=pinD;

    _bobNumber=(bobNumber>0)?bobNumber:1;

    outData = (unsigned char* ) malloc((sizeof(unsigned char))*bobNumber);

    pinMode(_pinD,OUTPUT);
    pinMode(_pinG,OUTPUT);
    pinMode(_pinSCLR, OUTPUT);
    pinMode(_pinRCK,OUTPUT);
    pinMode(_pinSCK,OUTPUT);

    reset();
}

void BOB::reset(){
    digitalWrite(_pinG,1);
    digitalWrite(_pinSCLR,0);
    digitalWrite(_pinRCK,0);
    digitalWrite(_pinSCLR,1);
    digitalWrite(_pinRCK,1);
    digitalWrite(_pinG,0);

    for(int i = 0; i< _bobNumber;i++){
        outData[i]=0x00;
    }
}

void BOB::write(int bobNumber, unsigned char value){
    if(!isBobNumberValid(bobNumber))return;

    outData[bobNumber]=value;

    sendDataToBob();
}

void BOB::writePin(unsigned char bobNumber, unsigned char pin, unsigned char value){
    if(!isBobNumberValid(bobNumber)) return;

    unsigned char bitMask = 0b00000001<<(pin&0x07);
    if(value==LOW){
        outData[bobNumber] &= ~bitMask;
    }
    else{
        outData[bobNumber] |= bitMask;
    }
    sendDataToBob();
}

void BOB::sendDataToBob(void){
    digitalWrite(_pinRCK,0);
    for(int bobNumber = _bobNumber-1;bobNumber>=0;bobNumber--){
        for(unsigned char i =0b00000001;i!=0b00000000;i<<=1){
            digitalWrite(_pinSCK,0);
            digitalWrite(_pinD,outData[bobNumber]&i);
            digitalWrite(_pinSCK,1);
        }
    }
    digitalWrite(_pinRCK,1);
}

bool BOB::isBobNumberValid(int bobNumber){
    return (bobNumber>=0)&&(bobNumber<_bobNumber);
}

BOB::~BOB(){
    free(outData);
}


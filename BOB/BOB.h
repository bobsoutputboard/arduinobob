#ifndef BOB_H
#define BOB_H

class BOB
{
public:
    BOB(int pinG, int pinRCK, int pinSCK, int pinSCLR, int pinD);
    BOB(int pinG, int pinRCK, int pinSCK, int pinSCLR, int pinD, unsigned int bobNumber);
    ~BOB();

    void write(int bobNumber, unsigned char value);
    void write(unsigned char value)
        {write(0,value);}

    void writePin(unsigned char bobNumber, unsigned char pin, unsigned char value);
    void writePin(unsigned char pin, unsigned char value)
        {writePin(0, pin, value);}

    void reset();

private:
    void sendDataToBob(void);
    bool isBobNumberValid(int bobNumber);
    unsigned char*  outData;
    int _bobNumber;
    int _pinD;
    int _pinG;
    int _pinSCLR;
    int _pinRCK;
    int _pinSCK;
};

#endif // BOB_H

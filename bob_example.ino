#include <BOB.h>

// crating a bob instance, with bob connected to pins 7,6,5,4,3 and only 1 bob (no chaining)
BOB bob(7,6,5,4,3,1);



// the setup routine runs once when you press reset:
void setup() {  
  //turn on some outputs on the first bob board
  bob.write(0,0b10100101);
}

// blinking outputs 2 and 5 on the first bob board
void loop() {
  delay(1000);
  bob.writePin(0,2,HIGH);
  bob.writePin(0,5,LOW);
  delay(1000);
  bob.writePin(0,5,HIGH);
  bob.writePin(0,2,LOW);
}
